import matplotlib.pyplot as plt
from glob import glob
import numpy as np
from PIL import Image
import random
import os
import csv

dir = "/data/dgominski/skraa/test/"
cid = None
cid2 = None
toDel = []
plotSize = None


def get_pick(event):
    global cid
    global toDel
    col = event.artist.colNum
    row = event.artist.rowNum
    idx = row * plotSize[1] + col + 1
    toDel.append(idx)


def get_keypress(event):
    global cid2

    if event.key == 'q':
        event.canvas.mpl_disconnect(cid2)
        plt.close('all')

    if event.key == 'd':
        # Delete selected images + rewrite CSV
        filestoDel = [samples[idx-1] for idx in toDel]
        treat_folder(folder, filestoDel)

    if event.key == 'g':
        # Delete whole folder after confirmation
        print()


def treat_folder(folder, filestoDel):
    csvfile = open(folder + "/params.csv", "r")
    csvoutput = open(folder + "/paramsnew.csv", "w")
    writer = csv.writer(csvoutput)
    for file in filestoDel:
        os.remove(file)
    for row in csv.reader(csvfile):
        if row[1] not in filestoDel:
            writer.writerow(row)


def plot_size_switcher(argument):
    switcher = {
        2: (1, 2),
        3: (1, 3),
        4: (2, 2),
        5: (2, 3),
        6: (2, 3),
        7: (2, 4),
        8: (2, 4),
        9: (3, 3),
        10: (3, 4),
        11: (3, 4),
        12: (3, 4),
        13: (4, 4),
        14: (4, 4),
        15: (4, 4),
        16: (4, 4),
        17: (3, 6),
        18: (3, 6),
        19: (4, 5),
        20: (4, 5),
        21: (5, 5),
        22: (5, 5),
        23: (5, 5),
        24: (5, 5),
        25: (5, 5)
    }
    return switcher.get(argument, (6, 6))


def display_images(images):
    global plotSize
    global cid
    global cid2
    fig = plt.figure(figsize=(100, 100))
    images = [Image.open(image) for image in images]
    for i in range(len(images)):
        plotSize = plot_size_switcher(len(images))
        plttemp = fig.add_subplot(*plotSize, i+1)
        plttemp.imshow(images[i])
        plttemp.set_picker(True)
    wm = plt.get_current_fig_manager()
    cid = fig.canvas.mpl_connect('pick_event', get_pick)
    cid2 = fig.canvas.mpl_connect('key_press_event', get_keypress)
    wm.window.wm_geometry("1920x1080+0+0")
    plt.show()


folders = glob(dir+"*")

greenlight = True
while greenlight:

    folder = random.choice(folders)
    images = glob(folder+"/*.jpg")
    images = np.array([image.split("/")[-1].split("_")[0] for image in images])
    classes = np.unique(images)

    for klass in classes:
        samples = glob(folder+"/"+klass+"*")
        display_images(samples)


