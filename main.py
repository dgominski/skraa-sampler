import pandas as pd
import numpy as np
import shapefile
from glob import glob
import os
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon as pltPolygon
from shapely.geometry.polygon import Polygon
from PIL import Image
Image.MAX_IMAGE_PIXELS = 1000000000000000
from mathutils import are4PointsInPolygon, compute_homography, isValid
import argparse
import time


align_pts = []
cid = None
cid2 = None
nb_candidates = 0
skip = False
data_path = "/data/dgominski/skraa/srcdata/aarhus/"
list_of_folders = glob(data_path + "*")
samples_path = '/data/dgominski/skraa/tmp/'
bootstrap = None
#bootstrap = np.array([554864, 6322600])
end = None
#end = np.array([555112, 6323700])


def get_click_location(event):
    global align_pts
    global cid
    global nb_candidates
    point = np.array([event.xdata, - event.ydata])
    align_pts.append(point)

    if len(align_pts) == nb_candidates:
        event.canvas.mpl_disconnect(cid)
        plt.close('all')


def get_keypress(event):
    global skip
    global cid2

    if event.key == 'q':
        skip = True
        event.canvas.mpl_disconnect(cid2)
        plt.close('all')


def plot_size_switcher(argument):
    switcher = {
        2: (1, 2),
        3: (1, 3),
        4: (2, 2),
        5: (2, 3),
        6: (2, 3),
        7: (2, 4),
        8: (2, 4),
        9: (3, 3),
        10: (3, 4),
        11: (3, 4),
        12: (3, 4),
        13: (4, 4),
        14: (4, 4),
        15: (4, 4),
        16: (4, 4),
        17: (3, 6),
        18: (3, 6),
        19: (4, 5),
        20: (4, 5),
        21: (5, 5),
        22: (5, 5),
        23: (5, 5),
        24: (5, 5),
        25: (5, 5)
    }
    return switcher.get(argument, (6, 6))


def align_images(images):
    global cid
    global cid2
    fig = plt.figure(figsize=(100, 100))
    for i in range(len(images)):
        fig.add_subplot(*plot_size_switcher(len(images)), i+1)
        plt.imshow(images[i])
    cid = fig.canvas.mpl_connect('button_press_event', get_click_location)
    cid2 = fig.canvas.mpl_connect('key_press_event', get_keypress)
    wm = plt.get_current_fig_manager()
    wm.window.wm_geometry("1920x1080+0+0")
    plt.show()


def get_samples(candidates, real_world_points, wanted_width, wanted_height):
    global skip
    if os.path.exists(
            samples_path + '{}{}'.format(real_world_points[0][0], real_world_points[0][1])):
        return

    regs2 = [compute_homography(trow, direction='real2im') for trow in candidates]
    regs2, images = zip(*regs2)

    overlap_pixels = [reg2.predict(real_world_points) for reg2 in regs2]

    bounding_boxes = [(np.min(pc[:, 0]), np.min(pc[:, 1]),
                       np.max(pc[:, 0]), np.max(pc[:, 1])) for pc in overlap_pixels]

    cropped_images = [images[0].crop(bounding_boxes[0])]
    cropped_images.extend([images[i].crop(bounding_boxes[i]).resize((cropped_images[0].width, cropped_images[0].height), resample=Image.LANCZOS)
                           for i in range(1, len(candidates))])

    # Now we have N images covering approximately the same zone. Get user input to align the images.
    align_images(cropped_images)

    if skip:
        skip = False
        return
    else:
        os.makedirs(
            samples_path + '{}{}'.format(real_world_points[0][0], real_world_points[0][1]))

    # cropped_images = [cropped_images[i].crop(usr_bounding_boxes[i]) for i in range(len(cropped_images))]

    # Compute the offsets for aligning images before final cropping
    # The reference is the first alignment point which should be the reference image
    x_offset = [0.0]
    y_offset = [0.0]
    for i in range(1, len(candidates)):
        x_offset.append(-align_pts[0][0] + align_pts[i][0])
        y_offset.append(align_pts[0][1] - align_pts[i][1])

    # # Get the ratio of the bounding boxes to deduce a global ratio for the whole images
    # for i in range(len(candidates)):
    #     x_ratio = (usr_bounding_boxes[0][0] - usr_bounding_boxes[0][2]) / \
    #               (usr_bounding_boxes[i+1][0] - usr_bounding_boxes[i+1][2])
    #     y_ratio = (usr_bounding_boxes[0][1] - usr_bounding_boxes[0][3]) / \
    #               (usr_bounding_boxes[i+1][1] - usr_bounding_boxes[i+1][3])

    csv = pd.DataFrame(columns=['name', 'altitude', 'omega', 'phi', 'kappa', 'sunAngle'])

    for w in range(0, cropped_images[0].width, wanted_width):
        for h in range(0, cropped_images[0].height, wanted_height):
            samples = [cropped_image.crop((w + x_offset[idx], h + y_offset[idx],
                                           w + wanted_width + x_offset[idx], h + wanted_height + y_offset[idx]))
                       for idx, cropped_image in enumerate(cropped_images)]

            if all(isValid(sample, wanted_height, wanted_width) for sample in samples):

                for im in range(len(samples)):
                    samples[im].save(
                        samples_path + '{}{}/{}{}_{}.jpg'
                        .format(real_world_points[0][0], real_world_points[0][1], int(w / wanted_width), int(h / wanted_height), im))

                    entryparams = {
                        'name': '{}{}_{}.jpg'.format(int(w / wanted_width), int(h / wanted_height), im),
                        'altitude': candidates[im].altitude,
                        'omega': candidates[im].omega,
                        'phi': candidates[im].phi,
                        'kappa': candidates[im].kappa,
                        'sunAngle': candidates[im].sunAngle,
                        'coordinates': real_world_points
                    }

                    csv = csv.append(entryparams, ignore_index=True)

    csv.to_csv(samples_path + '{}{}/params.csv'.format(real_world_points[0][0], real_world_points[0][1]))


def main():
    global nb_candidates
    global align_pts
    global bootstrap

    df = pd.DataFrame(columns=['path', 'A', 'B', 'C', 'D', 'easting', 'northing', 'altitude', 'omega', 'phi', 'kappa', 'sunAngle'])

    for folder in list_of_folders:

        # Get shapefile
        sf = shapefile.Reader(folder+"/Orientations/footprints")
        records = sf.records()
        shapes = sf.shapes()

        # Fill Dataframe
        for i, record in enumerate(records):
            # If the image is a calibration image, skip
            if 'Calibration' in record[0]:
                print('Calibration image dismissed')
                continue
            # Verify that the image actually exists
            if not os.path.isfile(folder+"/ImageDir/"+record[0]+".jpg"):
                raise FileNotFoundError('The image {} provided in the shapefile does not exist'.format(folder+"/ImageDir/"+record[0]+".jpg"))
            # If the shape is not rectangular, warning
            if len(shapes[i].points) > 5:
                print('Image with irregular ground print')
                entry = {
                    'path': folder + "/ImageDir/" + record[0] + ".jpg",
                    'A': (shapes[i].bbox[0], shapes[i].bbox[1]),
                    'B': (shapes[i].bbox[2], shapes[i].bbox[1]),
                    'C': (shapes[i].bbox[2], shapes[i].bbox[3]),
                    'D': (shapes[i].bbox[0], shapes[i].bbox[3]),
                    'easting': record[1],
                    'northing': record[2],
                    'altitude': record[3],
                    'omega': record[4],
                    'phi': record[5],
                    'kappa': record[6],
                    'sunAngle': record[25]
                }
                df = df.append(entry, ignore_index=True)
                continue
            # Verify that the points are consistent
            if not shapes[i].points[0] == shapes[i].points[4] :
                raise ValueError('The 5th point in the shapefile does not match the 1st point as expected')
            entry = {
                'path': folder+"/ImageDir/"+record[0]+".jpg",
                'A': shapes[i].points[0],
                'B': shapes[i].points[1],
                'C': shapes[i].points[2],
                'D': shapes[i].points[3],
                'easting': record[1],
                'northing': record[2],
                'altitude': record[3],
                'omega': record[4],
                'phi': record[5],
                'kappa': record[6],
                'sunAngle': record[25]
            }
            df = df.append(entry, ignore_index=True)

    # Now use the Dataframe to sample data

    # First define zones of acquisition
    # Get all points
    points = df.as_matrix(columns=df.columns[1:5])
    points = points.flatten()
    points = np.asarray(points.tolist())

    bottom_left_point = np.array([np.min(points[:, 0]), np.min(points[:, 1])]).astype(np.int32)
    top_right_point = np.array([np.max(points[:, 0]), np.max(points[:, 1])]).astype(np.int32)

    # x_span = np.max(points[:, 0]) - np.min(points[:, 0])
    # y_span = np.max(points[:, 1]) - np.min(points[:, 1])

    # x_res = np.floor(x_span / 100).astype(np.int32)
    # y_res = np.floor(y_span / 100).astype(np.int32)
    x_res = 300
    y_res = 300

    # Define the minimum size of the image wanted
    wanted_width = 512
    wanted_height = 512

    if bootstrap is not None:
        bottom_left_point = bootstrap

    if end is not None:
        top_right_point = end

    print(bottom_left_point, top_right_point)

    # Sample a zone, starting from the top left, browsing with a sliding window
    for x in range(bottom_left_point[0], top_right_point[0], x_res):
        for y in range(bottom_left_point[1], top_right_point[1], y_res):
            print(x, y)
            # Define the zone
            real_world_points = np.array([[x, y],
                              [x + x_res, y],
                              [x + x_res, y + y_res],
                              [x, y + y_res]])

            candidates = []
            omegas = []
            phis = []
            kappas = []

            # Now browse the pandas dataframe to find if there are other shapes containing this 4 points
            for index, row in df.iterrows():
                points = [row['A'], row['B'], row['C'], row['D']]
                isContained = are4PointsInPolygon(real_world_points, points)

                # If it's contained, store the row
                if isContained and not candidates:
                    candidates.append(row)
                    omegas.append(row['omega'])
                    phis.append(row['phi'])
                    kappas.append(row['kappa'])

                # elif isContained and ((np.min(np.abs(np.array(omegas) - row['omega'])) > threshold)
                #                     or (np.min(np.abs(np.array(phis) - row['phi'])) > threshold)
                #                     or (np.min(np.abs(np.array(kappas) - row['kappa'])) > threshold)):
                elif isContained:
                    candidates.append(row)
                    omegas.append(row['omega'])
                    phis.append(row['phi'])
                    kappas.append(row['kappa'])

            if len(candidates) >= 2:
                print("It's a match!")
                nb_candidates = len(candidates)
                get_samples(candidates, real_world_points, wanted_width, wanted_height)
                align_pts = []
                plt.close('all')
                continue


if __name__ == "__main__":
    main()
