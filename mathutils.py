from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import numpy as np
from PIL import Image
from sklearn import linear_model

def isPointInPolygon(point_coordinates, polygon_coordinates):
    point = Point(point_coordinates)
    polygon = Polygon(polygon_coordinates)
    return polygon.contains(point)


def are4PointsInPolygon(points, polygon_coordinates):
    # wrapper for iterating 4 times isPointInPolygon, using tuples for polygon

    return (isPointInPolygon(points[0], polygon_coordinates) &
            isPointInPolygon(points[1], polygon_coordinates) &
            isPointInPolygon(points[2], polygon_coordinates) &
            isPointInPolygon(points[3], polygon_coordinates))


def estimate_image2real_matrix(row):
    # returns a LinearRegression object linking the image designed in the row with real world coordinates

    A, B, C, D = [np.append(np.array(value), 0) for value in row.values[0, 1:5]]
    aerial_image = Image.open(row.values[0, 0])
    image_width, image_height = aerial_image.size

    # build our data for least square regression to find perspective matrix A
    Y = np.asarray((A, B, C, D))
    # switch to homogeneous coordinates
    altitude = row.values[0, 7]
    plane_position = np.array([row.values[0, 5], row.values[0, 6], altitude])
    distances = np.array([np.linalg.norm(plane_position - A),
                          np.linalg.norm(plane_position - B),
                          np.linalg.norm(plane_position - C),
                          np.linalg.norm(plane_position - D)])
    Y[:, 2] = distances

    X = np.array([[0, 0, 1], [image_width, 0, 1], [image_width, image_height, 1], [0, image_height, 1]])

    reg = linear_model.LinearRegression(normalize=True).fit(X, Y)

    return reg, image_width, image_height


def estimate_real2image_matrix(row):
    # returns a LinearRegression object linking the image designed in the row with real world coordinates

    A, B, C, D = [np.append(np.array(value), 0) for value in row.values[1:5]]
    aerial_image = Image.open(row.values[0])
    image_width, image_height = aerial_image.size

    # build our data for least square regression to find perspective matrix A
    Y = np.asarray((A, B, C, D))
    # switch to homogeneous coordinates
    altitude = row.values[7]
    plane_position = np.array([row.values[5], row.values[6], altitude])
    distances = np.array([np.linalg.norm(plane_position - A),
                          np.linalg.norm(plane_position - B),
                          np.linalg.norm(plane_position - C),
                          np.linalg.norm(plane_position - D)])
    Y[:, 2] = distances

    X = np.array([[0, 0, 1], [image_width, 0, 1], [image_width, image_height, 1], [0, image_height, 1]])

    reg = linear_model.LinearRegression(normalize=True).fit(Y, X)

    return reg, aerial_image


def compute_homography(row, direction='real2im'):
    """returns a LinearRegression object linking the image designed in the row with real world coordinates
    + the image read on disk and rotated with kappa
    For each pair of points, we have a system in the form of :
    (uia) = (h11*uib + h12*vib + h13*1)
    (via)   (h21*uib + h22*vib + h23*1)
    (1  )   (h31*uib + h32*vib + h33*1)

    To solve this, we rewrite the system as :
    Mh = 0
    with M = (uib vib 1   0   0   0   -uib*uia -vib*uia -uia)
             (0   0   0   uib vib 1   -uib*via -vib*via -via)

    and h = (h11 h12 h13 h21 h22 h23 h31 h32 h33)

    We use 4 points and set h33=1 to reduce M to a 8x8 matrix.
    Now we can invert M and find the desired relation (non-homogeneous linear solution).

    ##### If direction = 1 ==> input image coordinates (pixels), output real world coordinates
    ##### If direction = 2 ==> input real world coordinates, output image coordinates (pixels)
    """

    A, B, C, D = row.values[1:5]
    aerial_image = Image.open(row.values[0])
    # kappa is a position 10
    kappa = row.values[10]
    # # round to either 0, 90, 180, 270
    # kappa = int(90 * round(kappa / 90))
    # if kappa == 360 or kappa == 0:
    #     kappa = 0
    # else:
    #     aerial_image = aerial_image.rotate(kappa, expand=True)
    aerial_image = aerial_image.rotate(kappa, expand=True)
    image_width, image_height = aerial_image.size

    # Rearrange the points so that we have first point at top left, second at top right, and so on
    # first split between left and right
    points = np.array([A, B, C, D])
    x_frontier = ((np.max(points[:, 0]) - np.min(points[:, 0])) / 2) + np.min(points[:, 0])
    y_frontier = ((np.max(points[:, 1]) - np.min(points[:, 1])) / 2) + np.min(points[:, 1])

    top_left = points[np.where((points[:, 0] < x_frontier) & (points[:, 1] > y_frontier))][0]
    top_right = points[np.where((points[:, 0] > x_frontier) & (points[:, 1] > y_frontier))][0]
    bottom_right = points[np.where((points[:, 0] > x_frontier) & (points[:, 1] < y_frontier))][0]
    bottom_left = points[np.where((points[:, 0] < x_frontier) & (points[:, 1] < y_frontier))][0]

    if direction == 'real2im':
        M = np.array([
            [top_left[0], top_left[1], 1, 0, 0, 0, 0, 0],
            [0, 0, 0, top_left[0], top_left[1], 1, 0, 0],
            [top_right[0], top_right[1], 1, 0, 0, 0, -top_right[0] * image_width, -top_right[1] * image_width],
            [0, 0, 0, top_right[0], top_right[1], 1, 0, 0],
            [bottom_right[0], bottom_right[1], 1, 0, 0, 0, -bottom_right[0] * image_width, -bottom_right[1] * image_width],
            [0, 0, 0, bottom_right[0], bottom_right[1], 1, -bottom_right[0] * image_height, -bottom_right[1] * image_height],
            [bottom_left[0], bottom_left[1], 1, 0, 0, 0, 0, 0],
            [0, 0, 0, bottom_left[0], bottom_left[1], 1, -bottom_left[0] * image_height, -bottom_left[1] * image_height]
        ])

        b = np.array(
            [0, 0, image_width, 0, image_width, image_height, 0, image_height]
        )
    if direction == 'im2real':
        M = np.array([
            [0, 0, 1, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 1, 0, 0],
            [image_width, 0, 1, 0, 0, 0, -image_width * top_right[0], 0],
            [0, 0, 0, image_width, 0, 1, -image_width * top_right[1], 0],
            [image_width, image_height, 1, 0, 0, 0, -image_width * bottom_right[0], -image_height * bottom_right[0]],
            [0, 0, 0, image_width, image_height, 1, -image_width * bottom_right[1], -image_height * bottom_right[1]],
            [0, image_height, 1, 0, 0, 0, 0, -image_height * bottom_left[0]],
            [0, 0, 0, 0, image_height, 1, 0, -image_height * bottom_left[1]]
        ])

        b = np.array(
            [top_left[0], top_left[1], top_right[0], top_right[1], bottom_right[0], bottom_right[1], bottom_left[0], bottom_left[1]]
        )

    h = np.linalg.inv(M) @ b.T

    return Predictor2000(h, direction), aerial_image


class Predictor2000:
    def __init__(self, h, direction):
        self.direction = direction
        self.M = np.reshape(np.append(h, 1), (3, 3))
        self.offset = np.array([0.0, 0.0])

    def predict(self, x):
        x = x[:, :2]
        x = np.concatenate((x, np.ones((x.shape[0], 1))), axis=1)
        y = np.zeros_like(x)
        for i in range(x.shape[0]):
            y[i, :] = self.M @ x[i, :]

        # Rescaling
        y = y/y[:, 2, None]

        if self.offset[0] != 0.0 and self.offset[1] != 0.0:
            y[:, :2] += self.offset
            if np.max(self.offset) > 50000.0 and self.direction == 'real2im':
                raise ValueError('Hey watch out, looks like you are giving real world coordinates as offset when'
                                 ' I should receive image pixels. This Predictor2000 is in mode {}.'.format(
                    self.direction))
            if np.max(self.offset) < 50000.0 and self.direction == 'im2real':
                raise ValueError('Hey watch out, looks like you are giving image pixels as offset when'
                                 ' I should receive real_world_coordinates. This Predictor2000 is in mode {}.'.format(
                    self.direction))

        if np.max(x) > 50000.0 and self.direction == 'im2real':
            raise ValueError('Hey watch out, looks like you are giving real world coordinates in input when'
                             ' I should receive image pixels. This Predictor2000 is in mode {}.'.format(self.direction))
        if np.max(x) < 50000.0 and self.direction == 'real2im':
            raise ValueError('Hey watch out, looks like you are giving image pixels in input when'
                             ' I should receive real world coordinates. This Predictor2000 is in mode {}.'.format(self.direction))

        return y

    def set_offset(self, offset):
        self.offset = offset


def isValid(sample, wanted_height2, wanted_width2):
    """Returns True if the sample is considered valid"""
    bbox = sample.getbbox()
    if bbox is None:
        return False
    elif bbox[2] - bbox[0] != wanted_width2 \
            or bbox[3] - bbox[1] != wanted_height2:
        return False
    else:
        return True

def isDifferentFromOthers(input, threshold, *args):
    """returns True if the N-dimensional vector input is different from at least one vector in args
    regarding any dimension"""
    if input.ndim != 1:
        raise ValueError("Expecting a 1D array (vector)")

    length = input.size
    for arg in args:
        if arg.ndim != 1:
            raise ValueError("Expecting only 1D arrays (vectors) to compare")
        if length != arg.size:
            raise ValueError("Vector sizes not consistent")